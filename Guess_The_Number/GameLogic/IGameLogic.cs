﻿

namespace Guess_The_Number
{
    public interface IGameLogic
    {
        public void Run();
    }
}
