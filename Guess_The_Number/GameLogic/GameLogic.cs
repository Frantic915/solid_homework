﻿
using System.Configuration;

namespace Guess_The_Number
{
    public class GameLogic : IGameLogic
    {
        private readonly IUserInterface _userInterface;

        private int _solutionNumber;
        
        private int _attempt;
        private int _maxAttempts;
        public GameLogic(IUserInterface userInterface)
        {
            _userInterface = userInterface;

            Reset();
        }
        public void Run()
        {
            do
            {
                if (_attempt >= _maxAttempts)
                {
                    _userInterface.ShowMessage(DefaultMessages.looseMessage);
                    Reset();
                    break;
                }

                _userInterface.ShowMessage($"Осталось {_maxAttempts - _attempt} попыток.");

                if (CheckStep())
                {
                    _userInterface.ShowMessage(DefaultMessages.winMessage);
                    Reset();
                    break;
                }

                _attempt++;
            }
            while (true);
        }

        private bool CheckStep()
        {
            bool result = false;
            var userNumber = GetUserNumber();

            if(userNumber == _solutionNumber)
            {
                result = true;
            }
            else if (userNumber < _solutionNumber)
            {
                _userInterface.ShowMessage("Загаданное число больше");
            }
            else if (userNumber > _solutionNumber)
            {
                _userInterface.ShowMessage("Загаданное число меньше");
            }

            return result;
        }

        private int GetUserNumber()
        {
            _userInterface.ShowMessage("Введите число");
            
            var input = _userInterface.ReadInputLine();

            if(int.TryParse(input, out int result))
            {
                return result;
            }
            else
            {
                _userInterface.ShowMessage(DefaultMessages.wrongInput);
                return GetUserNumber();
            }
        }

        private void Reset()
        {
            _solutionNumber = Random.Shared.Next(
                int.Parse(ConfigurationManager.AppSettings["minNumber"] ?? "0"),
                int.Parse(ConfigurationManager.AppSettings["maxNumber"] ?? "0"));

            _maxAttempts = int.Parse(ConfigurationManager.AppSettings["attempts"] ?? "0");

            _attempt = 0;
        }
    }
}
