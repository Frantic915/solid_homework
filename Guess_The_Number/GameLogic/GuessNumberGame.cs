﻿

namespace Guess_The_Number
{
    public class GuessNumberGame : IGame
    {
        private readonly IUserInterface _userInterface;
        private readonly IGameLogic _gameLogic;

        public GuessNumberGame(IUserInterface userInterface, IGameLogic gameLogic)
        {
            _userInterface = userInterface;
            _gameLogic = gameLogic;
        }

        public async Task Start()
        {
            _userInterface.ShowMessage(DefaultMessages.welcomeMessage);
            _userInterface.ShowMessage(DefaultMessages.rulesMessage);

            ConsoleKey playAgain;
            do
            {
                _gameLogic.Run();
                if (!CheckForExit())
                    break;
            }
            while (true);
        }

        private bool CheckForExit()
        {
            _userInterface.ShowMessage(DefaultMessages.playAgainQuestion);
            var consoleKey = _userInterface.ReadKey();

            if (consoleKey == ConsoleKey.N || consoleKey == ConsoleKey.Y)
            {
                return consoleKey == ConsoleKey.Y;
            }
            else
            {
                _userInterface.ShowMessage(DefaultMessages.wrongInput);
                return CheckForExit();
            }
        }
    }
}
