﻿

namespace Guess_The_Number
{
    public interface IGame
    {
        public Task Start();
    }
}
