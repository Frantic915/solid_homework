﻿using Microsoft.Extensions.DependencyInjection;

namespace Guess_The_Number
{
    // DI principle implemented, by using container
    public static class ContainerBuilder
    {
        public static IServiceProvider Build()
        {
            var container = new ServiceCollection();

            container.AddTransient<IGame, GuessNumberGame>();
            container.AddTransient<IUserInterface, ConsoleInterface>();
            container.AddTransient<IGameLogic, GameLogic>();


            return container.BuildServiceProvider();
        }
    }
}
