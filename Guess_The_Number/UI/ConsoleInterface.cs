﻿
namespace Guess_The_Number
{
    public class ConsoleInterface : IUserInterface
    {
        public string ReadInputLine()
        {
            var input = Console.ReadLine();
            Console.WriteLine();
            return input;
        }

        public ConsoleKey ReadKey()
        {
            var input = Console.ReadKey();
            Console.WriteLine();
            return input.Key;
        }

        public void ShowMessage(string message)
        {
            Console.WriteLine(message);
            Console.WriteLine();
        }

    }
}
