﻿using System.Configuration;

namespace Guess_The_Number
{
    public static class DefaultMessages
    {
        public static readonly string welcomeMessage = $"Привет! это игра \"{ConfigurationManager.AppSettings["gameName"]}\"";

        public static readonly string rulesMessage = 
            $"Компьютер выбрал случайное число между " +
            $"{ConfigurationManager.AppSettings["minNumber"]} " +
            $"и " +
            $"{ConfigurationManager.AppSettings["maxNumber"]}.\n" +
            $"Задача игрока отгадать это число за " +
            $"{ConfigurationManager.AppSettings["attempts"]} " +
            $"попыток\n" +
            $"После каждого ввода числа будет появляться подсказка.";

        public static readonly string winMessage = "Вы угадали число!";
        public static readonly string looseMessage = "У вас закончились попытки.";
        public static readonly string playAgainQuestion = "Сыграть еше раз? y/n";
        public static readonly string wrongInput = "Неправильный ввод";
    }
}
