﻿
namespace Guess_The_Number
{
    public interface IUserInterface
    {
        public void ShowMessage(string message);

        public string ReadInputLine();

        public ConsoleKey ReadKey();
    }
}
