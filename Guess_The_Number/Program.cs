﻿using Guess_The_Number;


var container = ContainerBuilder.Build();

var game = container.GetService(typeof(IGame)) as IGame;

await game?.Start();
